const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
    },
    status: {
        type: String,
        required: true,
    },
    state: {
        type: String,
    },
    name: {
        type: String,
        require: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    pickup_address: {
        type: String,
        required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        height: {
            type: Number,
            required: true,
        },
        width: {
            type: Number,
            required: true,
        },
        length: {
            type: Number,
            required: true,
        },
    },
});

module.exports = {
    Load,
}