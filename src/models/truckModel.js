const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default: null,
    },
    status: {
        type: String,
    },
    type: {
        type: String,
        require: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    dimensions: {
        height: {
            type: Number,
            required: true,
        },
        width: {
            type: Number,
            required: true,
        },
        length: {
            type: Number,
            required: true,
        },
    },
    created_date: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = {Truck};
