const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserProfile = async (userId) => {
    const user = await User.findOne({_id: userId}).select('-password -__v');
    return user;
};

const changeUserPassword = async (userId, data) => {
    await User.findOneAndUpdate({_id: userId}, {password: await bcrypt.hash(data, 10)});
};


const deleteUser = async ( userId) => {
    await User.findOneAndRemove({_id: userId});
};

module.exports = {
    getUserProfile,
    changeUserPassword,
    deleteUser,
};
