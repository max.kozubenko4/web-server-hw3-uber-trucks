const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const {User} = require('../models/userModel');

const registration = async ({email, password, role}) => {
    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role,
    });
    await user.save();
};

const signIn = async ({email, password}) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new Error('Invalid email or password');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('Invalid email or password');
    }

    const token = jwt.sign({
        _id: user._id,
        email: user.email,
    }, 'secret');
    return token;
};

const resetPassword = async (email) => {
    let newPass = '0';

    for (let i = 0; i < 4; i++) {
        newPass += getRandomInt(1, 100);
    }

    const hashPassword = await bcrypt.hash(newPass, 10);

    await User.findOneAndUpdate({email: email}, {password: hashPassword});

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'test89275@gmail.com',
            pass: 'Test1515',
        },
    });

    const mailOptions = {
        from: 'ServiceNodeJS.gmail.com',
        to: email,
        subject: 'Sending Email using Node.js',
        html: `<h1>Welcome</h1><h2>Your password has been changed</h2><p>Your new password is ${newPass}</p>`,
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
};

const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return String(Math.floor(Math.random() * (max - min)) + min);
};

module.exports = {
    registration,
    signIn,
    resetPassword,
};
