const {Error} = require('mongoose');
const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

const stateArr = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
];

const getUserLoads = async (userId, limit, offset, status) => {
    let loads = null;
    if (status !== undefined) {
        loads = await Load.find({created_by: userId, status: status}).skip(offset).limit(limit);
    } else {
        loads = await Load.find({created_by: userId}).skip(offset).limit(limit);
    }

    if (loads.length === 0) {
        throw new Error('string');
    }
    return loads;
};

const getUserActiveLoads = async (userId) => {
    // eslint-disable-next-line max-len
    const loadsActive = await Load.findOne({assigned_to: userId}).select('-__v');
    return loadsActive;
};

const getUserLoadById = async (id) => {
    const load = await Load.findOne({_id: id});
    if (load.length === 0) {
        throw new Error('string');
    }
    return load;
};

const getUserLoadShippingById = async (userId, id) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'shipper') {
        throw new Error('Just shipper can add loads!');
    }

    const loadInfo = await Load.findById({_id: id});
    const truckInfo = await Truck.findOne({assigned_to: loadInfo.assigned_to});

    const resultLoad = Object.assign(loadInfo, truckInfo);

    return resultLoad;
};

const addLoadForUser = async ( userId, name, payload, pickup_address, delivery_address, dimensions) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'shipper') {
        throw new Error('Just shipper can add loads!');
    }

    const load = new Load({
        created_by: userId,
        assigned_to: 'qwerty',
        status: 'NEW',
        state: 'IS',
        name: name,
        payload: payload,
        pickup_address: pickup_address,
        delivery_address: delivery_address,
        dimensions: dimensions,
    });

    await load.save();
};

const changeToNextLoadState = async (userId) => {
    const loadState = await Load.findOne({assigned_to: userId});

    if (loadState.state === stateArr[0]) {
        await Load.findOneAndUpdate({assigned_to: userId}, {state: stateArr[1]});
        return stateArr[1];
    } else if (loadState.state === stateArr[1]) {
        await Load.findOneAndUpdate({assigned_to: userId}, {state: stateArr[2]});
        return stateArr[2];
    } else if (loadState.state === stateArr[2]) {
        await Load.findOneAndUpdate({assigned_to: userId}, {state: stateArr[3], status: 'SHIPPED'});
        await Truck.findOneAndUpdate({assigned_to: userId}, {status: 'IS'});
        return stateArr[3];
    }
};

const postUserLoadById = async (userId, id) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'shipper') {
        throw new Error('Just shipper can add loads!');
    }

    const load = await Load.findByIdAndUpdate({_id: id}, {status: 'POSTED'});
    // Get Truck that have driver
    const trucks = await Truck.find();
    let flagFindDriver = false;

    for (let i = 0; i < trucks.length; i++) {
        if (trucks[i].assigned_to !== null && trucks[i].status === 'IS') {
            // eslint-disable-next-line max-len
            if (trucks[i].dimensions.length >= load.dimensions.length && trucks[i].dimensions.width >= load.dimensions.width && trucks[i].dimensions.height >= load.dimensions.height && trucks[i].payload >= load.payload) {
                flagFindDriver = true;
                // eslint-disable-next-line max-len
                await Load.findByIdAndUpdate({_id: id}, {status: 'ASSIGNED', assigned_to: trucks[i].assigned_to, state: stateArr[0]});
                // eslint-disable-next-line max-len
                await Truck.findByIdAndUpdate({_id: trucks[i]._id}, {status: 'OL'});
            }
        }
    }

    if (flagFindDriver === false) {
        await Load.findByIdAndUpdate({_id: id}, {status: 'NEW'});
        throw new Error('string');
    }
};

const updateLoadById = async (userId, id, name, payload, pickup_address, delivery_address, dimensions) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'shipper') {
        throw new Error('Just shipper can add loads!');
    }

    // eslint-disable-next-line max-len
    const result = await Load.findOneAndUpdate({_id: id, status: 'NEW'}, {name, payload, pickup_address, delivery_address, dimensions});

    if (result === null) {
        throw new Error('string');
    }
};

const deleteLoadById = async (userId, id) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'shipper') {
        throw new Error('Just shipper can delete loads!');
    }

    const result = await Load.findOneAndDelete({_id: id, status: 'NEW'});

    if (result === null) {
        throw new Error('string');
    }
};

module.exports = {
    getUserLoads,
    getUserActiveLoads,
    getUserLoadById,
    getUserLoadShippingById,
    addLoadForUser,
    changeToNextLoadState,
    postUserLoadById,
    updateLoadById,
    deleteLoadById,
};
