const { Error } = require('mongoose');
const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');

const trucksInfo = [
    {
        type: 'SPRINTER',
        payload: 1700,
        dimensions: {
            height: 300,
            width: 250,
            length: 170,
        },
    },
    {
        type: 'SMALL STRAIGHT',
        payload: 2500,
        dimensions: {
            height: 500,
            width: 250,
            length: 170,
        },
    },
    {
        type: 'LARGE STRAIGHT',
        payload: 4000,
        dimensions: {
            height: 700,
            width: 350,
            length: 200,
        },
    },
];

const getTrucksInfo = async (userId) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'driver') {
        throw new Error('Just driver can add trucks!');
    }

    const trucks = await Truck.find({created_by: userId}).select('-dimensions -payload -__v');
    return trucks;
};

const getTruckInfo = async (userId, truckId) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'driver') {
        throw new Error('Just driver can add trucks!');
    }

    const truck = await Truck.findOne({_id: truckId, created_by: userId}).select('-dimensions -payload -__v');
    return truck;
};

const addTruck = async (userId, truckType) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'driver') {
        throw new Error('Just driver can add trucks!');
    } else if (truckType === undefined) {
        throw new Error('Should be type of truck');
    }

    const truck = new Truck({
        created_by: userId,
        assigned_to: null,
        status: 'IS',
        type: truckType,
    });

    for (let i = 0; i < trucksInfo.length; i++) {
        if (trucksInfo[i].type === truckType) {
            truck.payload = trucksInfo[i].payload;
            truck.dimensions = trucksInfo[i].dimensions;
        }
    }


    await truck.save();
};

const assingTruckToDriver = async (userId, truckId) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'driver') {
        throw new Error('Just driver can add trucks!');
    }

    const trucks = await Truck.find({created_by: userId, assigned_to: userId});
    if (trucks.length === 0) {
        // eslint-disable-next-line max-len
        await Truck.findOneAndUpdate({_id: truckId, created_by: userId}, {assigned_to: userId});
    } else {
        throw new Error('The driver is assigned to other truck');
    }
};

const updatetruckInfo = async (userId, truckId, type) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'driver') {
        throw new Error('Just driver can update trucks info!');
    }

    for (let i = 0; i < trucksInfo.length; i++) {
        if (trucksInfo[i].type === type) {
            // eslint-disable-next-line max-len
            const answer = await Truck.findOneAndUpdate({_id: truckId, assigned_to: null, created_by: userId}, {type: type, dimensions: trucksInfo[i].dimensions, payload: trucksInfo[i].payload});
            if (answer === null) {
                throw new Error('string');
            }
        }
    }
};

const deleteTruckInfo = async (userId, truckId) => {
    const user = await User.findOne({_id: userId});

    if (user.role.toLowerCase() !== 'driver') {
        throw new Error('Just driver can update trucks info!');
    }

    // eslint-disable-next-line max-len
    const result = await Truck.findOneAndRemove({_id: truckId, assigned_to: null, created_by: userId});
    if (result === null) {
        throw new Error('string');
    }
};

module.exports = {
    getTrucksInfo,
    getTruckInfo,
    addTruck,
    assingTruckToDriver,
    updatetruckInfo,
    deleteTruckInfo,
};
