const express = require('express');
const router = express.Router();

const {
    getUserLoads,
    getUserActiveLoads,
    getUserLoadById,
    getUserLoadShippingById,
    addLoadForUser,
    changeToNextLoadState,
    postUserLoadById,
    updateLoadById,
    deleteLoadById,
} = require('../services/loadService');

const {
    asyncWrapper,
} = require('../utils/apiUtils');

const {
    InvalidRequestError,
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const status = req.query.status;
    const limit = parseInt(req.query.limit);
    const offset = parseInt(req.query.offset);

    const loads = await getUserLoads(userId, limit, offset, status);

    res.json({loads});
}));

router.get('/active', asyncWrapper(async (req, res) => {
    const {userId} = req.user;

    const load = await getUserActiveLoads(userId);

    res.json({load});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const {id} = req.params;

    const load = await getUserLoadById(id);

    res.json({load});
}));

router.get('/:id/shipping_info', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;

    const load = await getUserLoadShippingById(userId, id);

    res.json({load});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

    await addLoadForUser(userId, name, payload, pickup_address, delivery_address, dimensions);

    res.json({message: 'Load created successfully'});
}));

router.post('/:id/post', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;

    await postUserLoadById(userId, id);

    res.json({message: 'Load posted successfully', driver_found: true});
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
    const {userId} = req.user;

    const currentState = await changeToNextLoadState(userId);

    res.json({message: `Load state changed to '${currentState}'`});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

    await updateLoadById(userId, id, name, payload, pickup_address, delivery_address, dimensions);

    res.json({message: 'Load details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;

    await deleteLoadById(userId, id);

    res.json({message: 'Load deleted successfully'});
}));

module.exports = {
    loadRouter: router,
};
