const express = require('express');
const router = express.Router();

const {
    getUserProfile,
    changeUserPassword,
    deleteUser,
} = require('../services/userService');

const {
    asyncWrapper,
} = require('../utils/apiUtils');

const {
    InvalidRequestError,
} = require('../utils/errors');

router.get('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const user = await getUserProfile(userId);

    if (!user) {
        throw new InvalidRequestError('No user with such id found!');
    }

    res.json({user});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {newPassword} = req.body;
    await changeUserPassword(userId, newPassword);

    res.json({message: 'User`s password updated successfully'});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;

    await deleteUser(userId);

    res.json({message: 'User account deleted successfully'});
}));

module.exports = {
    userRouter: router,
};
