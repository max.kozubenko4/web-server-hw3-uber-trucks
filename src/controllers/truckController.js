const express = require('express');
const router = express.Router();

const {
    getTrucksInfo,
    getTruckInfo,
    addTruck,
    assingTruckToDriver,
    updatetruckInfo,
    deleteTruckInfo,
} = require('../services/truckService');

const {
    asyncWrapper,
} = require('../utils/apiUtils');

const {
    InvalidRequestError,
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const {userId} = req.user;

    const trucks = await getTrucksInfo(userId);

    res.json({trucks});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;

    const truck = await getTruckInfo(userId, id);

    res.json({truck});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {type} = req.body;

    await addTruck(userId, type);

    res.json({message: 'Truck has been created successfully!'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
    const {id} = req.params;
    const {userId} = req.user;

    await assingTruckToDriver(userId, id);

    res.json({message: 'Truck assigned successfully!'});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;
    const {type} = req.body;
    await updatetruckInfo(userId, id, type);

    res.json({message: 'Truck details changed successfully!'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {id} = req.params;

    await deleteTruckInfo(userId, id);

    res.json({message: 'Truck deleted successfully!'});
}));

module.exports = {
    truckRouter: router,
};
