const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
    const schema = Joi.object({
        password: Joi.string()
            .min(3)
            .max(20)
            .required(),
        email: Joi.string()
            .email()
            .required(),
        role: Joi.string()
            .required(),
    });

    try {
        await schema.validateAsync(req.body);
        next();
    } catch (err) {
        next(err);
    }
};

module.exports = {
    registrationValidator,
};
