const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;
require('dotenv').config();

const {authRouter} = require('./controllers/authController');
const {userRouter} = require('./controllers/userController');
const {truckRouter} = require('./controllers/truckController');
const {loadRouter} = require('./controllers/loadController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {NodeCourseError} = require('./utils/errors');


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/trucks', [authMiddleware], truckRouter);
app.use('/api/loads', [authMiddleware], loadRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'});
});
app.use((err, req, res, next) => {
    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://uberTruck:uberTruck@cluster0.qsokx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true,
        });
        app.listen(port);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
};

start();
